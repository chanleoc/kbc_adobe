"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2017'"
"__project__ = 'kbc_adobe'"

"""
Python 3 environment 
"""


import sys
import os
import logging
import csv
#import json
import ujson as json
#import simplejson as json
import pandas as pd
import datetime
import dateparser

import logging_gelf.formatters
import logging_gelf.handlers
from keboola import docker

### destination to fetch and output files
DEFAULT_FILE_INPUT = "/data/in/tables/"
DEFAULT_FILE_DESTINATION = "/data/out/tables/"


class _process():
    """
    Handling all the data processing fetched from Adobe Analytics API
    """

    output = pd.DataFrame()
    fetched_report = []
    not_fetched_report = []

    def __init__(self, file_name):
        self.file_name = file_name

    def data_process(self, reportID, text):
        """
        Processing data depending on the Request Method
        """

        ### Initilizing tables and list to store data and column names
        columns = ["date"]
        fetched_report = self.fetched_report
        not_fetched_report = self.not_fetched_report

        ### Turning text into JSON formatted data
        data = json.loads(text)
       
        ### Check if the processing was fine
        ### Breaking component if error is found from the API request
        if "error" in data:
            #raise Exception ("error: {0}, error_description: {1}".format(data["error"],data["error_description"]))
            logging.error(data)
            logging.info("{0}: {1}".format(reportID, data["error_description"]))
            not_fetched_report.append(reportID)

            if data["error"] == "report_id_invalid":
                return False, True
            else:
                return False, False

        ### Append Elements
        elements = []
        for i in data["report"]["elements"]:
            columns.append(i["name"])
            elements.append(i["name"])

        ### Append Metrics
        metrics = []
        for i in data["report"]["metrics"]:
            columns.append(i["name"]+"/"+i["id"])
            metrics.append(i["name"]+"/"+i["id"])
        logging.info("Report Elements: {0}".format(elements))
        logging.info("Report Metrics: {0}".format(metrics))

        ### Looping thru the Date Granularity
        ### "breakdown" should exist within the date granularity level, 
        ###     if not, component will empty row for that date
        for i in data["report"]["data"]:
            ### "breakdown"
            if "breakdown" in i:
                for k in i["breakdown"]:
                    out = {}
                    out["date"]=i["name"]
                    self.breakdown_loop(k, out, columns[1:])
                    
            else:
                out = {}
                out["date"]=i["name"]
                self.output = self.output.append(out, ignore_index=True)
        
        ### Preset the order of the output table columns
        columns.append("url")

        ### Recording the list of fetched reportID
        fetched_report.append(reportID)

        ### Replacing existing class variable with updated ones
        self.fetched_report = fetched_report
        self.not_fetched_report = not_fetched_report
        self.columns = columns
        self.elements = elements

        return True, False

    def breakdown_loop(self, data, out, columns):
        """
        Parsing logic of the JSON properties and JSON breakdowns
        """
        
        ### Verifying the consistency in the data return
        if len(columns)==0:
            logging.warning("Return results are weird. Number of metrics and elemtns do not add up. - {0}".format(data["name"]))

        ### When "breakdown" is found within the JSON Object
        ### Recursively run the function to break down the properties within the breakdown
        if "breakdown" in data:
            ### Element's Value
            column_name = columns[0]
            out[column_name] = data["name"]

            if "url" in data:
                ### Concat all the URL within the same breakdown
                ### API should logically only return URl when element, page, is entered as a parameter
                if "url" in out:
                    out["url"] = out["url"]+data["url"]
                else:
                    out["url"] = data["url"]
            ### Running function breakdown_loop if breakdown is found within the properties        
            for i in data["breakdown"]:
                self.breakdown_loop(i, out, columns[1:])
        
        ### When "breakdown" is not found
        ### Break off the count values with metrics as properties
        else:
            ### Element's Value
            out[columns[0]]=data["name"]
            columns = columns[1:]

            ### Nested with no breakdowns (Empty elements)
            ### Results inequality in number of columns(elements+metrics) and number of counts
            if len(columns)!= len(data["counts"]):
                logging.warning("{0} - Does not have any breakdowns.".format(data["name"]))
                elements = columns[:(len(columns)-len(data["counts"]))]
                metrics = columns[(len(columns)-len(data["counts"])):]
                
                for i in elements:
                    ### Elements will be left empty
                    out[i] = ""
                itr = 0
                for i in metrics:
                    ### Metrics value will be based on the returned counts' values
                    out[i] = data["counts"][itr]
                    itr+=1

            ### Bottom of the recursion
            ### Left with values of metrics fetching from the "counts" property
            else:
                count=0
                if "url" in data:
                    if "url" in out:
                        out["url"] = out["url"]+data["url"]
                    else:
                        out["url"] = data["url"]
                while count < len(columns):
                    column_name = columns[count]
                    data_value = data["counts"][count]
                    out[column_name] = data_value
                    count+=1

            ### Append into the Global Output    
            self.output = self.output.append(out, ignore_index=True)

        return

    def produce_manifest(self, file_name, primary_key):
        """
        Function to return header per file type.
        Manifest to handle return/calculated/export data file
        """

        file = "/data/out/tables/"+str(file_name)+".manifest"
        destination_part = file_name.split(".csv")[0]

        manifest_template = {#"source": "myfile.csv"
                            #,"destination": "in.c-mybucket.table"
                            #"incremental": True
                            #,"primary_key": ["VisitID","Value","MenuItem","Section"]
                            #,"columns": [""]
                            #,"delimiter": "|"
                            #,"enclosure": ""
                            }

        column_header = []

        manifest = manifest_template
        primary_key_mod = ["date"]
        for i in primary_key:
            i = i.replace(" ","_")
            i = i.replace("_-_","_")
            i = i.replace("_-","_")
            i = i.replace("-_","_")
            i = i.replace("__","_")
            primary_key_mod.append(i)
        manifest["primary_key"] = primary_key_mod
        
        manifest["incremental"] = True

        try:
            with open(file, 'w') as file_out:
                json.dump(manifest, file_out)
                logging.info("Output manifest file ({0}) produced.".format(file_name))
        except Exception as e:
            logging.error("Could not produce output file manifest.")
            logging.error(e)
        
        return

    def file_output(self):
        """
        Outputting the files
        """

        ### destination to fetch and output files
        DEFAULT_FILE_INPUT = "/data/in/tables/"
        DEFAULT_FILE_DESTINATION = "/data/out/tables/"
            
        output = self.output
        if output.empty:
            ### Verify if the output contains any data
            ### If empty, output nothing
            logging.info("There are no reports to output.")
            return

        else:
            ### PK of the file
            elements = self.elements

            output.to_csv(DEFAULT_FILE_DESTINATION+self.file_name+".csv", index=False)
            self.produce_manifest(self.file_name+".csv", elements)