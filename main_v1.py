"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2017'"
"__project__ = 'kbc_adobe'"

"""
Python 3 environment 
"""


import sys
import os
import logging
import csv
import json
import pandas as pd
import datetime
import dateparser

import logging_gelf.formatters
import logging_gelf.handlers
from keboola import docker

###Local Import
from _adobe_requests import _request as rq
import _adobe_queue as aq
import _adobe_process as ap

### Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

### Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")

logger = logging.getLogger()
logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
    host=os.getenv('KBC_LOGGER_ADDR'),
    port=int(os.getenv('KBC_LOGGER_PORT'))
    )
logging_gelf_handler.setFormatter(logging_gelf.formatters.GELFFormatter(null_character=True))
logger.addHandler(logging_gelf_handler)

# removes the initial stdout logging
logger.removeHandler(logger.handlers[0])


### Access the supplied rules
cfg = docker.Config('/data/')
params = cfg.get_parameters()
application_id = cfg.get_parameters()["application_id"]
application_secret = cfg.get_parameters()["#application_secret"]
file_name = cfg.get_parameters()["file_name"]
api = cfg.get_parameters()["api"]
from_date = cfg.get_parameters()["from_date"]
to_date = cfg.get_parameters()["to_date"]

request_config_temp = cfg.get_parameters()["request_config"]
### adding top parameters
### top: adding the number of returns of the elements, default: 10, max: 50000
for i in request_config_temp["elements"]:
    i["top"] = 50000
request_config = {}
request_config["reportDescription"] = request_config_temp

logging.info("Adobe API: {0}".format(api))
logging.info("Request Body: {0}".format(request_config))

### Get proper list of tables
cfg = docker.Config('/data/')
in_tables = cfg.get_input_tables()
out_tables = cfg.get_expected_output_tables()
logging.info("IN tables mapped: "+str(in_tables))

### destination to fetch and output files
DEFAULT_FILE_INPUT = "/data/in/tables/"
DEFAULT_FILE_DESTINATION = "/data/out/tables/"



def get_tables(in_tables):
    """
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """

    ### input file
    table = in_tables[0]
    in_name = table["full_path"]
    in_destination = table["destination"]
    logging.info("Data table: " + str(in_name))
    logging.info("Input table source: " + str(in_destination))
    
    return in_name

def read_status_file(file_name):
    """
    Parsing Status File
    """

    headers = ["request_id", "report_name", "request_date", "status", "extracted_date"]
    if file_name == "":
        empty_df = pd.DataFrame(columns=headers)
        return empty_df
    else:
        input_file = pd.read_csv(file_name, dtype=str)#, header=headers)
        return input_file

def dates_request(start_date, end_date):
    """
    return a list of dates within the given parameters
    """
    dates = []
    try:
        start_date_form = dateparser.parse(start_date)
        end_date_form = dateparser.parse(end_date)
        day_diff = (end_date_form-start_date_form).days
        if day_diff < 0:
            raise Exception ("ERROR: from_date cannot exceed to_date. Please check your input settings for from_date and to_date.")
            #logging.error("Exit.")
            sys.exit(1)
        temp_date = start_date_form
        day_n = 0
        if day_diff == 0:
            ### normal with leading zeros
            dates.append(temp_date.strftime("%Y-%m-%d"))
            ### no leading zeros
            #dates.append(temp_date.strftime("%Y/%-m/%-d"))
        while day_n < day_diff:
            ### normal with leading zeros 
            dates.append(temp_date.strftime("%Y-%m-%d"))
            ### no leading zeros
            #dates.append(temp_date.strftime("%Y/%-m/%-d"))
            temp_date += datetime.timedelta(days=1)
            day_n += 1
            if day_n == day_diff:
                ### normal with leading zeros
                dates.append(temp_date.strftime("%Y-%m-%d"))
                ### no leading zeros
                #dates.append(temp_date.strftime("%Y/%-m/%-d"))
    except TypeError:
        logging.error("ERROR: Please enter valid date parameters")
        logging.error("Exit.")
        sys.exit(1)

    return dates

def reproduce_manifest(file_name):
        """
        Reproducing input file manifest to output file manifest 
        if changes are made to the input file_name.
        """

        input_file = "/data/in/tables/"+str(file_name)+".manifest"
        output_file = "/data/out/tables/"+str(file_name)+".manifest"
        destination_part = file_name.split(".csv")[0]

        with open(input_file) as f:
            input_data = json.load(f)

        manifest_template = {#"source": "myfile.csv"
                            "destination": input_data["id"]
                            ,"incremental": True
                            ,"primary_key": input_data["primary_key"]
                            #,"columns": input_data["columns"]
                            #,"delimiter": "|"
                            #,"enclosure": ""
                            }

        column_header = []

        manifest = manifest_template
        
        try:
            with open(output_file, 'w') as file_out:
                json.dump(manifest, file_out)
                logging.info("Output manifest file ({0}) produced.".format(file_name))
        except Exception as e:
            logging.error("Could not produce output file manifest.")
            logging.error(e)

        return

def produce_manifest(file_name, primary_key):
        """
        Function to return header per file type.
        """

        file = "/data/out/tables/"+str(file_name)+".manifest"
        destination_part = file_name.split(".csv")[0]

        manifest_template = {#"source": "myfile.csv"
                            #,"destination": "in.c-mybucket.table"
                            #"incremental": True
                            #,"primary_key": ["VisitID","Value","MenuItem","Section"]
                            #,"columns": [""]
                            #,"delimiter": "|"
                            #,"enclosure": ""
                            }

        column_header = []

        manifest = manifest_template
        
        manifest["primary_key"] = primary_key
        #manifest["incremental"] = True

        try:
            with open(file, 'w') as file_out:
                json.dump(manifest, file_out)
                logging.info("Output manifest file ({0}) produced.".format(file_name))
        except Exception as e:
            logging.error("Could not produce output file manifest.")
            logging.error(e)
        
        return

def main():
    """
    Main execution script.
    """
    
    ### Get Phase ###
    ### Token Request
    access_token = rq.token_request(application_id, application_secret)

    """ Handle Request Date Phase """
    ### Handling request file
    try:
        in_table = get_tables(in_tables)
        new_status_file = False
    except Exception:
        in_table = ""
        new_status_file = True

    intable = read_status_file(in_table)
    ### Filter input table by requested file_name
    table_filter_temp = intable.loc[intable["report_name"]==file_name]
    
    ### Filter input table with no "fetched" status
    table_filter = table_filter_temp.loc[table_filter_temp["status"]!="fetched"]
    
    ### Figure out the dates to queue the Configurations
    date_range = dates_request(from_date, to_date)

    ### To prevent same date is queued again
    ### Filtering out reportID based on the configured report_name
    date_to_queue = list(set(date_range) - set(table_filter_temp["request_date"].tolist()))
    logging.info("List of dates to Queue: {0}".format(date_to_queue))
    
    if not date_to_queue:
        logging.info("There are no dates to queue.")


    """ Date Queue Request """
    queuing = aq._queue(date_range=date_to_queue, request_config=request_config, report_name=file_name)
    queuing.in_queue(access_token=access_token)

    ### Validate if there is any data needed to be queue
    ### if none, keep runing
    if queuing.out_json:
        ### Table with all the reportID data
        intable = intable.append(queuing.out_json, ignore_index=True)
        ### Filtered table waiting to be fetched
        table_filter = table_filter.append(queuing.out_json, ignore_index=True)
        logging.info("List of reportID to Get: {0}".format(table_filter["request_id"].tolist()))    


    """ Data Get Request """
    output = ap._process(file_name=file_name)
    current_date = dateparser.parse("today")
    ### Setting the upper limit of the component to avoid container exceeding timeout
    table_filter = table_filter.head(21)

    for index, row in table_filter.iterrows():
        if str(row["status"]).lower()== "nan" or str(row["status"]) == "":
            ### Json Config to request report
            get_request_config = {
                "reportID": row["request_id"]
            }

            ### Data Request
            result = rq.main_request(access_token, "Get", get_request_config)
            
            ### Data Process & Output
            out_bool = output.data_process(reportID=row["request_id"], text=result)

            if out_bool:
                ### Determine if the request was successful or not
                ### if found, replacing status and the extraction_date
                intable.loc[intable["request_id"]==row["request_id"], "status"] = "fetched"
                intable.loc[intable["request_id"]==row["request_id"], "extracted_date"] = current_date.strftime("%Y-%m-%d")


    """ Output """
    ### Handling output of the status and the manifest of the status
    intable.to_csv(DEFAULT_FILE_DESTINATION+"status.csv", index=False)  
    if not new_status_file:
        reproduce_manifest("status.csv")
    else:
        #_status = ap._process("status.csv")
        pk = ["request_id", "report_name", "request_date"]
        #_status.produce_manifest("status.csv", pk)
        produce_manifest("status.csv", pk)

    ### Outputting data requested from "Get"
    output.file_output()
    logging.info("reportID Fetched: {0}".format(output.fetched_report))
    logging.info("reportID Not Fetched: {0}".format(output.not_fetched_report))


    return


if __name__ == "__main__":
    
    main()

    logging.info("Done.")
