"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2017'"
"__project__ = 'kbc_adobe'"

"""
Python 3 environment 
"""


import sys
import os
import logging
#import json
import ujson as json
#import simplejson as json
import pandas as pd
import datetime
import dateparser

import logging_gelf.formatters
import logging_gelf.handlers
from keboola import docker

###Local Import
from _adobe_requests import _request as rq

class _queue:
    """
    Processes to Queue the request configuration in Adobe Analytics
    """

    def __init__(self, date_range, request_config, report_name):
        self.date_range = date_range 
        self.request_config = request_config
        self.report_name = report_name
        self.out_json = []

    def in_queue(self, access_token):
        """
        Queue the report with the given date and request_config
        - replacing the date in the request_config
        """

        ### Parameters
        out_json = self.out_json
        for i in self.date_range:
            request_config = self.request_config

            json_template = {
                "request_id": "",
                "request_date": str(i),
                "status": "",
                "extracted_date": "",
                "report_name": self.report_name
            }

            ### Preparing parameters for queuing
            request_config["reportDescription"]["date"] = i

            ### Queue Request
            result = rq.main_request(access_token, "Queue", request_config)
            result_json = json.loads(result)
            
            ### Appending API results into Status Sheet
            json_template["request_id"] = str(result_json["reportID"])
            out_json.append(json_template)

        self.out_json = out_json



