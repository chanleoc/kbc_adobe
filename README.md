# Keboola Adobe Analytics #

### Introduction ###
This repository is a collection of functions which allow users to queue and get their desired reports from [Adobe Analytics](https://sc.omniture.com/login/) Reporting API. It allows users to explore, manipulate and enrich their data piror any data analytsis. With KBC orchestration, users can automate the process of data extraction and data transformation with a specified schedule. It is recommended for user to possess the knowledge of queuing their reports in their Adobe ReportSuite prior using this application.


### Adobe Application Registration Instructions ###

1. Login to [Adobe Developer](https://marketing.adobe.com/developer)
2. Click Development Tools -> Applications
3. Click Add
4. New Application Configurations
    1. Select "Client Credentials"
    2. Fill in Application Name
    3. Select your organization
    4. Select "Report" as your scope
    5. Application ID and Application Secret will be in the application


### Report Status Tracking ###

Component uses a status file from input mapping to keep track of the list of reports which are being queued and fetched. User is expected to have one "Adobe Report" configuration per extractor configuration. To avoid duplicate request on the same day, the output file name and the requested date are used to keep track of the requests being processed by the component. Everytime a new configuration with different date is queued, it will be added into the sheet with a new requested_id. If the requested report is not ready to be fetched, the status of that row will become empty; on the other hand, it will be "fetched" if the requested report is found.

If the status file is found within the input mapping, component will automatically update with new values and export incrementally back into where the status file came from. Meanwhile, if this is the first time you running the appliation or having an empty the input mapping, the component will then treat this run as an initial run of that configuration and export a new "status.csv" to a default bucket of the component assigned by KBC.


### Configuration ###

1. Application ID
    - Adobe Analytics Application ID
    - To register an application, please follow [here](https://marketing.adobe.com/developer/documentation/authentication-1/auth-register-app-1#concept_83C0BB1A226F44C5BF6B08A2D43B34D5). Registration instructions are listed above.

2. Application Secret
    - Adobe Analytics Secret

3. Adobe API
    - API Scope which the component will be requesting from
    - Component only supports Report API at the moment

4. Output File Name
    - The table name which will be used in the output file in the output bucket
    - Output Bucket will be decided by KBC

5. From Date
    - The starting date used to run the report
    - Date Format: YYYY-MM-DD

6. To Date
    - The ending date used to run the report
    - Date Format: YYYY-MM-DD

7. Request Configuration
    1. reportSuiteID
        - The Analytics report suite you want to use to generate the report
    2. dateGranularity
        - The time units used to display data in a report
        - Options:
            1. day
            2. hour
            3. week
            4. month
            5. quarter
            6. year
    3. elements
        - List of elements that breaks down the metrics data in the report
        - Adobe API can only support a maximum number of three elements
        - With default API settings, it only returns 10 rows per elements. Due to the nature of the API, the component has been configure to have a top 50000 which means it will have a maximum of 50000 rows per elements.
        - **Please ensure the entered elements are a valid breakdowns in the report**
    4. metrics
        - The metircs to include in the report
        - Must specify at least one metric

**Detailed Adobe Analytics Reporting API can be found [here](https://marketing.adobe.com/developer/documentation/sitecatalyst-reporting/c-overview-1)**


### RAW JSON Configuration ###

```
{
    "application_id": "adobe_application_id",
    "#application_secret": "adobe_application_secret",
    "file_name": "output_somewhere",
    "api": "Report",
    "from_date": "today",
    "to_date": "today",
    "request_config":{
        "reportSuiteID": "which_report",
        "dateGranularity": "day",
        "elements": [
            {
            "id": "element_1", "top": 50000
            },
            {
            "id": "element_2", "top": 50000
            }
        ],
        "metrics": [
            {
            "id": "metric_1"
            }
        ]
        }
}
```

### Contact Info ###

Leo Chan  
Vancouver, Canada (PST time)  
Email: leo@keboola.com  
Private: cleojanten@hotmail.com  
