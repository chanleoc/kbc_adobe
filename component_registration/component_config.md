A collection of integrated online marketing and Web analytics products

### Adobe Application Registration ###
Please follow steps in [here](https://bitbucket.org/chanleoc/kbc_adobe/src/master/)

### Input Description ###

1. Application ID
    - Adobe Analytics Application ID

2. Application Secret
    - Adobe Analytics Secret

3. Adobe API
    - API Scope which the component will be requesting from
    - Component only supports Report API at the moment

4. Output File Name
    - The table name which will be used in the output file in the output bucket
    - Output Bucket will be selected by KBC

5. From Date
    - The starting date used to run the report
    - Date Format: YYYY-MM-DD

6. To Date
    - The ending date used to run the report
    - Date Format: YYYY-MM-DD

7. Request Configuration
    1. reportSuiteID
        - The Analytics report suite you want to use to generate the report
    2. dateGranularity
        - The time units used to display data in a report
    3. elements
        - List of elements that breaks down the metrics data in the report
        - Adobe API can only support a maximum number of three elements
        - With default API settings, it only returns 10 rows per elements. Due to the nature of the API, the component has been configure to have a top 50000 which means it will have a maximum of 50000 rows per elements.
        - **Please ensure the entered elements are a valid breakdowns in the report**
    4. metrics
        - The metircs to include in the report
        - Must specify at least one metric

Configuration details can be found [here](https://bitbucket.org/chanleoc/kbc_adobe/src/master/)

Docker Version: 0.0.3