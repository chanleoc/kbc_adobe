"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2017'"
"__project__ = 'kbc_adobe'"

"""
Python 3 environment 
"""

#import json
import ujson as json
#import simplejson as json
import requests
from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session 
import logging


api = "Report"

class _request():
    """
    Handling all the requests to Adobe Analytics API
    """

    def token_request(application_id, application_secret):
        """
        Access Token Request
        """

        token_url = "https://api.omniture.com/token"
        header = {}

        client = BackendApplicationClient(client_id=application_id)
        oauth = OAuth2Session(client=client)
        token = oauth.fetch_token(
            token_url=token_url,
            client_id=application_id,
            client_secret=application_secret
        )

        if token["access_token"]:
            logging.info("Access Token Granted.")
            access_token = token["access_token"]
        else:
            raise Exception ("Failed to fetch access token. Please check your credentials or your access privilege.")

        return access_token

    def main_request(access_token, api_method, request_config):
        """
        Main Request based on Configured Method and API
        """

        base_url = "https://api.omniture.com/admin/1.4/rest/?method="+api+"."+api_method+"&access_token="+access_token
        body = json.dumps(request_config)
       
        ### Main Request body 
        result = requests.post(base_url, data=body) 
        if api_method.lower() == "get":
            logging.info("ReportID: {0}; Requests Status: {1}".format(request_config["reportID"],result.status_code))
        elif api_method.lower() == "queue":
            logging.info("Queue Date: {0}; Request Status: {1}".format(request_config["reportDescription"]["date"], result.status_code))
        
        return result.text